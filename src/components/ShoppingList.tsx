import { useAutoAnimate } from '@formkit/auto-animate/react'

interface Props {
  children: JSX.Element | JSX.Element[]
}

export const ShoppingList: React.FC<Props> = ({ children }) => {
  const [parent] = useAutoAnimate(/* optional config */)

  return (
    <ul className="item-list" ref={parent}>
      {children}
    </ul>
  )
}
