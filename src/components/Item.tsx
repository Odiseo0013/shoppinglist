import { useEffect, useRef, useState } from 'react'
import { type item as ItemType } from '../types'
import { IconButton } from '@mui/material'
import DeleteIcon from '@mui/icons-material/Delete'

interface Props extends ItemType {
  id: string
  title: string
  onRemoveItem: (id: string) => void
  onPurchasedItem: ({ id, purchased }: Pick<ItemType, 'id' | 'purchased'>) => void
  setTitle: (params: { id: string; title: string }) => void
  isEditing: string
  setIsEditing: (purchased: string) => void
}

export const Item: React.FC<Props> = ({
  id,
  title,
  purchased,
  onPurchasedItem,
  setTitle,
  onRemoveItem,
  isEditing,
  setIsEditing
}) => {
  const [editedTitle, setEditedTitle] = useState(title)
  const inputEditTitle = useRef<HTMLInputElement>(null)

  const handleKeyDown: React.KeyboardEventHandler<HTMLInputElement> = e => {
    if (e.key === 'Enter') {
      setEditedTitle(editedTitle.trim())

      if (editedTitle !== title) {
        setTitle({ id, title: editedTitle })
      }

      if (editedTitle === '') onRemoveItem(id)
      setIsEditing('')
    }

    if (e.key === 'Escape') {
      setEditedTitle(title)
      setIsEditing('')
    }
  }

  useEffect(() => {
    inputEditTitle.current?.focus()
  }, [isEditing])

  const handleChangeCheckbox = (event: React.ChangeEvent<HTMLInputElement>): void => {
    onPurchasedItem({
      id,
      purchased: event.target.checked
    })
  }

  return (
    <>
      <div className="view">
        <input className="toggle" type="checkbox" checked={purchased} onChange={handleChangeCheckbox} />
        <label
          style={{ cursor: 'text' }}
          onClick={() => {
            setIsEditing(id)
          }}
        >
          {title}
        </label>
        <IconButton
          aria-label="delete"
          onClick={() => {
            onRemoveItem(id)
          }}
          className="destroy"
        >
          <DeleteIcon />
        </IconButton>
        {/* <button
          className="destroy"
          onClick={() => {
            onRemoveItem(id)
          }}
        ></button> */}
      </div>
      <input
        className="edit"
        value={editedTitle}
        onChange={e => {
          setEditedTitle(e.target.value)
        }}
        onKeyDown={handleKeyDown}
        onBlur={() => {
          setIsEditing('')
        }}
        ref={inputEditTitle}
      />
    </>
  )
}
