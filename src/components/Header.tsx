import { type ItemTitle } from '../types'
import CreateItem from './CreateItem'

interface Props {
  onAddItem: ({ title }: ItemTitle) => void
}

export const Header: React.FC<Props> = ({ onAddItem }) => {
  return (
    <header className="header">
      <CreateItem saveItem={onAddItem} />
    </header>
  )
}

export default Header
