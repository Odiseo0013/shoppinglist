import { Button } from '@mui/material'
import { FILTER_BUTTONS } from '../consts'
import { type FilterValue } from '../types'

interface Props {
  onFilterChange: (filter: FilterValue) => void
  filterSelected: FilterValue
  purchasedCount: number
}

export const Filter: React.FC<Props> = ({ filterSelected, onFilterChange, purchasedCount }) => {
  return (
    <ul className="filters">
      {Object.entries(FILTER_BUTTONS).map(([key, { href, label }]) => {
        const isSelected = key === filterSelected
        const className = isSelected ? 'selected' : ''

        return (
          <li key={key} style={{ margin: '0px 10px 0px 0px' }}>
            {/* <a
              href={href}
              className={`${className} ${purchasedCount}`}
              onClick={event => {
                event.preventDefault()
                onFilterChange(key as FilterValue)
              }}
            >
              {label}
            </a> */}

            <Button
              className={`${className} ${purchasedCount}`}
              onClick={event => {
                event.preventDefault()
                onFilterChange(key as FilterValue)
              }}
              variant="outlined"
              size="small"
            >
              {label}
            </Button>
          </li>
        )
      })}
    </ul>
  )
}
