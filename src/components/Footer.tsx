import { Button, Typography } from '@mui/material'
import { type FilterValue } from '../types'
import { Filter } from './Filter'
import DeleteForeverIcon from '@mui/icons-material/DeleteForever'

interface Props {
  pendingCount: number
  purchasedCount: number
  filterSelected: FilterValue
  onClearPurchased: () => void
  onFilterChange: (filter: FilterValue) => void
}

export const Footer: React.FC<Props> = ({
  pendingCount,
  purchasedCount,
  onClearPurchased,
  filterSelected,
  onFilterChange
}) => {
  return (
    <footer
      className="footer"
      style={{ display: 'flex', justifyContent: 'space-between', height: '36px', alignItems: 'center' }}
    >
      <Typography variant="caption" className="todo-count">
        <strong>{pendingCount}</strong> pending
      </Typography>
      <Filter filterSelected={filterSelected} onFilterChange={onFilterChange} purchasedCount={purchasedCount} />
      {purchasedCount > 0 ? (
        <Button
          startIcon={<DeleteForeverIcon />}
          className="clear-completed"
          onClick={onClearPurchased}
          variant="text"
          size="small"
        >
          Purchased
        </Button>
      ) : (
        ' '
      )}
    </footer>
  )
}
