import { useState } from 'react'
import { type ItemTitle } from '../types'

interface Props {
  saveItem: ({ title }: ItemTitle) => void
}

export const CreateItem: React.FC<Props> = ({ saveItem }) => {
  const [inputValue, setInputValue] = useState('')

  const handleSubmit = (event: React.FormEvent<HTMLFormElement>): void => {
    event.preventDefault()
    saveItem({ title: inputValue })
    setInputValue('')
  }

  return (
    <form onSubmit={handleSubmit}>
      <input
        className="new-item"
        placeholder="Add new item"
        value={inputValue}
        onChange={evt => {
          setInputValue(evt.target.value)
        }}
        autoFocus={true}
      />
    </form>
  )
}

export default CreateItem
