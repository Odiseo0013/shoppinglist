export const ITEMS_FILTERS = {
  ALL: 'all',
  PENDING: 'pending',
  PURCHASED: 'purchased'
} as const

export const FILTER_BUTTONS = {
  [ITEMS_FILTERS.ALL]: {
    label: 'All',
    href: `/?filter=${ITEMS_FILTERS.ALL}`
  },
  [ITEMS_FILTERS.PENDING]: {
    label: 'Pending',
    href: `/?filter=${ITEMS_FILTERS.PENDING}`
  },
  [ITEMS_FILTERS.PURCHASED]: {
    label: 'Purchased',
    href: `/?filter=${ITEMS_FILTERS.PURCHASED}`
  }
} as const
