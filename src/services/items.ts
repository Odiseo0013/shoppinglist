import { type listOfItems } from '../types'

const API_URL = 'https://api.jsonbin.io/v3/b/65e7b460dc74654018ae7adb'

interface ImportMetaEnv {
  readonly VITE_API_KEY: string
  // more env variables...
}

// interface ImportMeta {
//   readonly env: ImportMetaEnv
// }

interface item {
  id: string
  title: string
  purchased: boolean
  order: number
}

export const fetchItems = async (): Promise<item[]> => {
  const res = await fetch(API_URL, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'X-Master-Key': '$2a$10$5J45RFZO1YYqVnOE7AkkFOf2ZxLHEKq6qjpXHvub0NxUPOFPThZcK'
    }
  })
  if (!res.ok) {
    console.error('Error fetching Items')
    return []
  }
  const { record: items } = (await res.json()) as { record: item[] }
  return items
}

export const updateItems = async ({ items }: { items: listOfItems }): Promise<boolean> => {
  const res = await fetch(API_URL, {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
      'X-Master-Key': '$2a$10$5J45RFZO1YYqVnOE7AkkFOf2ZxLHEKq6qjpXHvub0NxUPOFPThZcK'
    },

    body: JSON.stringify(items)
  })

  return res.ok
}
