import { useEffect, useReducer, useState } from 'react'
import { ITEMS_FILTERS } from '../consts'
import { fetchItems, updateItems } from '../services/items'
import { type listOfItems, type FilterValue, type ItemTitle, type item } from '../types'

const initialState = {
  sync: false,
  items: [],
  filterSelected: (() => {
    const params = new URLSearchParams(window.location.search)
    const filter = params.get('filter') as FilterValue | null
    if (filter === null) return ITEMS_FILTERS.ALL
    return Object.values(ITEMS_FILTERS).includes(filter) ? filter : ITEMS_FILTERS.ALL
  })()
}

type Action =
  | { type: 'INIT_ITEMS'; payload: { items: listOfItems } }
  | { type: 'CLEAR_PURCHASED' }
  | { type: 'PURCHASED'; payload: { id: string; purchased: boolean } }
  | { type: 'FILTER_CHANGE'; payload: { filter: FilterValue } }
  | { type: 'REMOVE'; payload: { id: string } }
  | { type: 'SAVE'; payload: { title: string } }
  | { type: 'UPDATE_TITLE'; payload: { id: string; title: string } }

interface State {
  sync: boolean
  items: listOfItems
  filterSelected: FilterValue
}

const reducer = (state: State, action: Action): State => {
  if (action.type === 'INIT_ITEMS') {
    const { items } = action.payload

    return {
      ...state,
      sync: false,
      items
    }
  }

  if (action.type === 'CLEAR_PURCHASED') {
    return {
      ...state,
      sync: true,
      items: state.items.filter(item => !item.purchased)
    }
  }

  if (action.type === 'PURCHASED') {
    const { id, purchased } = action.payload.id
    return {
      ...state,
      sync: true,
      items: state.items.map(item => {
        if (item.id === id) {
          return {
            ...item,
            purchased
          }
        }

        return item
      })
    }
  }

  if (action.type === 'FILTER_CHANGE') {
    const { filter } = action.payload
    return {
      ...state,
      sync: true,
      filterSelected: filter
    }
  }

  if (action.type === 'REMOVE') {
    const { id } = action.payload
    return {
      ...state,
      sync: true,
      items: state.items.filter(item => item.id !== id)
    }
  }

  if (action.type === 'SAVE') {
    const { title } = action.payload.title
    const newItem = {
      id: crypto.randomUUID(),
      title,
      purchased: false
    }

    return {
      ...state,
      sync: true,
      items: [...state.items, newItem]
    }
  }

  if (action.type === 'UPDATE_TITLE') {
    const { id, title } = action.payload
    return {
      ...state,
      sync: true,
      items: state.items.map(item => {
        if (item.id === id) {
          return {
            ...item,
            title
          }
        }

        return item
      })
    }
  }

  return state
}

export const useItemReduce = (): {
  pendingCount: number
  purchasedCount: number
  items: listOfItems
  filterSelected: FilterValue
  handleRemoveAllPurchased: () => void
  handlePurchased: ({ id, purchased }: Pick<ItemType, 'id' | 'purchased'>) => void
  handleFilterChange: (filter: FilterValue) => void
  handleRemove: (id: string) => void
  handleAddItem: ({ title }: ItemTitle) => void
  handleUpdateTitle: (params: { id: string; title: string }) => void
  isLoading: boolean
} => {
  const [{ sync, items, filterSelected }, dispatch] = useReducer(reducer, initialState)

  const [isLoading, setIsLoading] = useState(false)

  const handlePurchased = (id: string, purchased: boolean): void => {
    dispatch({ type: 'PURCHASED', payload: { id, purchased } })
  }

  const handleRemove = (id: string): void => {
    dispatch({ type: 'REMOVE', payload: { id } })
  }

  const handleUpdateTitle = ({ id, title }: { id: string; title: string }): void => {
    dispatch({ type: 'UPDATE_TITLE', payload: { id, title } })
  }

  const handleAddItem = (title: string): void => {
    dispatch({ type: 'SAVE', payload: { title } })
  }

  const handleRemoveAllPurchased = (): void => {
    dispatch({ type: 'CLEAR_PURCHASED' })
  }

  const handleFilterChange = (filter: FilterValue): void => {
    dispatch({ type: 'FILTER_CHANGE', payload: { filter } })

    const params = new URLSearchParams(window.location.search)
    params.set('filter', filter)
    window.history.pushState({}, '', `${window.location.pathname}?${params.toString()}`)
  }

  const filteredItems = items.filter(item => {
    if (filterSelected === ITEMS_FILTERS.PENDING) {
      return !item.purchased
    }

    if (filterSelected === ITEMS_FILTERS.PURCHASED) {
      return item.purchased
    }

    return true
  })

  const purchasedCount = items.filter(item => item.purchased).length
  const pendingCount = items.length - purchasedCount

  useEffect(() => {
    setIsLoading(true)
    fetchItems()
      .then(items => {
        dispatch({ type: 'INIT_ITEMS', payload: { items } })
        setIsLoading(false)
      })
      .catch(err => {
        console.error(err)
      })
  }, [])

  useEffect(() => {
    if (sync) {
      updateItems({ items }).catch(err => {
        console.error(err)
      })
    }
  }, [items, sync])

  return {
    pendingCount,
    purchasedCount,
    filterSelected,
    handleRemoveAllPurchased,
    handlePurchased,
    handleFilterChange,
    handleRemove,
    handleAddItem,
    handleUpdateTitle,
    isLoading,
    items: filteredItems
  }
}
