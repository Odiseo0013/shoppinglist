import { useState } from 'react'
import { ITEMS_FILTERS } from '../consts'
import { type listOfItems, type FilterValue, type item as ItemType, type ItemTitle } from '../types'

const mockItems = [
  {
    id: '1',
    title: 'Manzanas',
    purchased: true
  },
  {
    id: '2',
    title: 'Peras',
    purchased: false
  },
  {
    id: '3',
    title: 'Patatas',
    purchased: false
  }
]

export const useItems = (): {
  pendingCount: number
  purchasedCount: number
  items: listOfItems
  filterSelected: FilterValue
  handleRemoveAllPurchased: () => void
  handlePurchased: ({ id, purchased }: Pick<ItemType, 'id' | 'purchased'>) => void
  handleFilterChange: (filter: FilterValue) => void
  handleRemove: (id: string) => void
  handleAddItem: ({ title }: ItemTitle) => void
  handleUpdateTitle: ({ id, title }: { id: string; title: string }) => void
} => {
  const [items, setItems] = useState(mockItems)
  const [filterSelected, setFilterSelected] = useState<FilterValue>(() => {
    // read from url query params using URLSearchParams
    const params = new URLSearchParams(window.location.search)
    const filter = params.get('filter') as FilterValue | null
    if (filter === null) return ITEMS_FILTERS.ALL
    // check filter is valid, if not return ALL
    return Object.values(ITEMS_FILTERS).includes(filter) ? filter : ITEMS_FILTERS.ALL
  })

  const handlePurchased = ({ id, purchased }: Pick<ItemType, 'id' | 'purchased'>): void => {
    const newItems = items.map(item => {
      if (item.id === id) {
        return {
          ...item,
          purchased
        }
      }

      return item
    })

    setItems(newItems)
  }

  const handleAddItem = ({ title }: ItemTitle): void => {
    const newItem = {
      id: crypto.randomUUID(),
      purchased: false,
      title
    }
    const newItems = [...items, newItem]
    setItems(newItems)
  }

  const handleRemove = (id: string): void => {
    const newItems = items.filter(item => item.id !== id)
    setItems(newItems)
  }

  const handleRemoveAllPurchased = (): void => {
    const newItems = items.filter(item => !item.purchased)
    setItems(newItems)
  }

  const handleUpdateTitle = ({ id, title }: { id: string; title: string }): void => {
    const newItems = items.map(item => {
      if (item.id === id) {
        return {
          ...item,
          title
        }
      }
      return item
    })

    setItems(newItems)
  }

  const handleFilterChange = (filter: FilterValue): void => {
    setFilterSelected(filter)
    const params = new URLSearchParams(window.location.search)
    params.set('filter', filter)
    window.history.pushState({}, '', `${window.location.pathname}?${params.toString()}`)
  }

  const purchasedCount = items.filter(item => item.purchased).length
  const pendingCount = items.length - purchasedCount

  const filteredItems = items.filter(item => {
    if (filterSelected === ITEMS_FILTERS.PENDING) {
      return !item.purchased
    }

    if (filterSelected === ITEMS_FILTERS.PURCHASED) {
      return item.purchased
    }

    return true
  })

  return {
    pendingCount,
    purchasedCount,
    filterSelected,
    handleRemoveAllPurchased,
    handlePurchased,
    handleFilterChange,
    handleRemove,
    handleAddItem,
    handleUpdateTitle,
    items: filteredItems
  }
}
