import { Card, CircularProgress, Typography } from '@mui/material'
import { ShoppingList, Footer, Header, Item } from './components'
import { useItemReduce } from './hooks/useItemReduce'
import { useState } from 'react'

const App = (): JSX.Element => {
  const {
    pendingCount,
    purchasedCount,
    filterSelected,
    handleRemoveAllPurchased,
    handlePurchased,
    handleFilterChange,
    handleRemove,
    handleAddItem,
    handleUpdateTitle,
    isLoading,
    items: filteredItems
  } = useItemReduce()

  const [isEditing, setIsEditing] = useState('')
  const filteredItemsArray = Object.values(filteredItems)

  return (
    <>
      <Typography variant="h1" fontSize="3rem">
        Shopping List Component
      </Typography>
      <Card className="shoppingList">
        <Header onAddItem={handleAddItem} />
        <ShoppingList>
          {filteredItemsArray.length === 0 && isLoading ? (
            <span className="loading-container">
              <CircularProgress />
            </span>
          ) : (
            <>
              {filteredItemsArray.map(item => (
                <li
                  key={item.id}
                  className={`regular${item.purchased ? ' completed' : ''}${isEditing === item.id ? ' editing' : ''}`}
                >
                  <Item
                    key={item.id}
                    id={item.id}
                    title={item.title}
                    purchased={item.purchased}
                    onRemoveItem={handleRemove}
                    onPurchasedItem={handlePurchased}
                    setTitle={handleUpdateTitle}
                    isEditing={isEditing}
                    setIsEditing={setIsEditing}
                  />
                </li>
              ))}
              {filteredItemsArray.length > 0 ? null : (
                <Typography
                  variant="h6"
                  component="h3"
                  sx={{
                    textAlign: 'left',
                    fontStyle: 'italic',
                    color: 'lightgrey',
                    minHeight: '100px',
                    height: '100px',
                    display: 'flex',
                    padding: '16px !important',
                    justifyContent: 'center',
                    alignItems: 'center'
                  }}
                >
                  No items to display
                </Typography>
              )}
            </>
          )}
        </ShoppingList>
        <Footer
          pendingCount={pendingCount}
          purchasedCount={purchasedCount}
          filterSelected={filterSelected}
          onClearPurchased={handleRemoveAllPurchased}
          onFilterChange={handleFilterChange}
        />
      </Card>
    </>
  )
}

export default App
