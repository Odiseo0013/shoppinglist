import { type ITEMS_FILTERS } from './consts'

export interface item {
  id: string
  title: string
  purchased: boolean
}

export type ItemTitle = Pick<item, 'title'>

export type listOfItems = item[]

export type FilterValue = (typeof ITEMS_FILTERS)[keyof typeof ITEMS_FILTERS]
